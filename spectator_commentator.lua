--[[
NOTE The terms team = Player and allyTeam = teams. Just a SpringRTS thing be aware that i will use normal specifies, not SpringRTS.
Confusing terminology explained here LINK https://beyond-all-reason.github.io/spring/articles/team-terminology

The six expected (max teams) are abstracted by greek letters, so they are more differentiated from players.
]]

local widget_name = "Spectator Commentator"
local widget_version = "0.1α"
local do_debug = true -- NOTE runs the script also as non spec.

---@alias path <string> path
---@alias file <string> file
---@alias set {[string]: boolean} A set like table, with only true boolean; returns nil or true.

function widget:GetInfo() ---@diagnostic disable-line: duplicate-set-field
    return {
        name = widget_name,
        desc = "Gives light (audio) comments to the player while spectating.",
        author = "Nehroz",
        date = "2024.12.1", -- update date.
        license = "GPL v3",
        layer = 0,
        enabled = true,
        handler = true,
        version = widget_version
    }
end

local AUDIO_FOLDER = "LuaUI/Widgets/Sounds//SpectatorCommentator/" ---@type path
local NUM_PLAYERS_SUPPORTED = 16
local NUM_TEAMS_SUPPORTED = 6
local GREEK_ALPHABET = {"alpha", "beta", "gamma", "delta", "epsilon", "zeta"}
local AUDIO_LEN = VFS.Include(AUDIO_FOLDER .. "index.lua") ---@type {file:number}
local GLOBAL_CD = 10 -- 10sec before a trigger can be repeated

-- Spring call wraps
local get_player_info = Spring.GetPlayerInfo

-- SECTION HELPER FUNCS
local function print(stuff)
    Spring.Echo(stuff)
end

---@source LINK https://github.com/beyond-all-reason/Beyond-All-Reason/blob/master/luarules/gadgets/sfx_notifications.lua#L20
local function PlayersInAllyTeamID(allyTeamID)
    local players = Spring.GetPlayerList()
	local _,_,spec,_,allyTeam
	for ct, id in pairs(players) do
		_,_,spec,_,allyTeam = get_player_info(id,false)
		if not spec and allyTeam ~= allyTeamID then
			players[ct] = nil
		end
	end
	return players
end

---returns a randomly chosen entry from a table
---@param iterable table
---@return unknown
local function choice(iterable)
    return iterable[math.random(1,#iterable)]
end


---Returns a table of uIDs with filter function applied.
---@param filter fun(uDef:table): boolean
---@return integer[]
local function get_unit_with_filter(filter)
    local all = Spring.GetAllUnits()
    local hit = {} ---@type integer[]
    for _, uID in pairs(all) do
        local uDef = UnitDefs[Spring.GetUnitDefID(uID)]
        if uDef then
            if filter(uDef) then
                table.insert(hit, uID)
            end
        end
    end
    return hit
end

-- !SECTION HELPER FUNCS
-- SECTION HANDLING
-- SECTION MESSAGE
---@param text string
local function create_chat_msg(text)
    Spring.SendMessageToPlayer(Spring.GetMyPlayerID(), text)
end
-- !SECTION MESSAGE
-- SECTION MARKERS
local function ping(x,y,z, text)
    Spring.MarkerAddPoint(x, y, z, text, true)
end

local function ping_unit(uID, text)
    local x, y, z = Spring.GetUnitPosition(uID)
    ping(x,y,z, text)
end
-- !SECTION MARKERS
-- SECTION AUDIO
local do_audio = true -- toggle
local audio_buffer = {} ---@type file[]
local audio_timer = 0. -- length of audio file playing
local time_since_last_sound = 0. -- timer
local audio_volume = 0.8 -- volume

---plays audio and returns true if sound is played
---@param file file
---@return boolean
local function play_sound(file)
    if time_since_last_sound > audio_timer then
        if AUDIO_LEN[file] then
            Spring.PlaySoundFile(AUDIO_FOLDER .. file, audio_volume, 'ui')
            time_since_last_sound = 0
            audio_timer = AUDIO_LEN[file]
            return true
        end
    end
    return false
end

---handles the audio buffer
local function try_playback()
    time_since_last_sound = time_since_last_sound + 1
    if #audio_buffer == 0 then return end
    local r = play_sound(audio_buffer[#audio_buffer])
    if r then
        table.remove(audio_buffer)
    end
end

---@param name file
local function add_audio_stack(name)
    if do_audio then
        table.insert(audio_buffer, name)
    end
end
-- !SECTION AUDIO
-- !SECTION HANDLING
--[[
TODO[epic=feat] Add T2 and T3 first reached.
> check created units for tech level, first hit activates guard
TODO[epic=feat] catch AFUS death.
> check dead units for a specific tag on AFUS
TODO[epic=feat] catch COMM rez.
> Not sure how this works; probably counts as new unit.
TODO[epic=feat] catch Team Defeat.
> team data?
TODO[epic=feat] catch Player elimination.
> Same as team; check further, probably can be ignored.
]]
-- NOTE Tracking tables here
local comms = {} ---@type integer[] holds uIDs of commanders
local comm_tagged = {} ---@type set for already pinged low_hp comms
local team_table = {} ---@type {integer:integer[]} Each team id and what players (teams) it holds
local players_team = {}

-- SECTION INITIALIZATION
function widget:Initialize() ---@diagnostic disable-line: duplicate-set-field
    --print(Spring.GetSpectatingState())
    if Spring.GetSpectatingState() == false and not do_debug then
        widget:Shutdown() -- TODO this is a nul ... for some reason.
    end
    print(widget_name .. " V" .. widget_version .. " loaded.")


    comms = get_unit_with_filter(function (uDef)
        return uDef.springCategories and uDef.springCategories.commander == true end
    )

    for i=0, NUM_TEAMS_SUPPORTED-1, 1 do
        local team_mates = Spring.GetTeamList(i)
        if team_mates then
            for _, v in pairs(team_mates) do
                players_team[v] = i
            end
        end
    end
    --print(AUDIO_LEN)
    --print(players_team)
end

local do_start = true
function widget:Update(_delta)
    if do_start then
        -- First message
        if Spring.GetGameSeconds() == 0 then
            add_audio_stack(choice({"strategy_phase.wav", "commanders_are_picking_there_drop_locations.wav"}))
        end
        do_start = false
    end
end
-- !SECTION INITIALIZATION
-- SECTION RUNTIME
function widget:GameFrame(frame) ---@diagnostic disable-line: duplicate-set-field
    if do_audio then try_playback() end

    -- Check Comm HP
    for _, uID in pairs(comms) do
        local hp = Spring.GetUnitHealth(uID)
        local def = UnitDefs[Spring.GetUnitDefID(uID)]
        if def and hp then
            if hp < (def.health/2) then
                if not comm_tagged[uID] then
                    local p = Spring.GetUnitTeam(uID) +1
                    ping_unit(uID, "Low HP Commander from Player " .. tostring(p))
                    add_audio_stack("commander_in_danger.wav")
                    comm_tagged[uID] = true
                end
            end
            if comm_tagged[uID] then
                if hp > (def.health*0.7) then
                    comm_tagged[uID] = nil
                end
            end
        end
    end
end

function widget:UnitDestroyed(uID, uIDDef, playerID, attackerID, attackerDefID, attackerPlayerID, weaponDefID) ---@diagnostic disable-line: duplicate-set-field
    -- handle Commanders
    if comm_tagged[uID] then
        comm_tagged[uID] = nil
        for i = #comms, 1, -1 do if comms[i] == uID then
            table.remove(comms, i)
            ping_unit(uID, "Commander from Player " .. tostring(playerID +1).. " downed")
            add_audio_stack("commander_was_destroyed.wav")
        end end
    end
end


function widget:UnitCreated(uID, uDefID, playerID, builderID)
    if UnitDefs[uDefID].customParams.iscommander then
        print("added")
        table.insert(comms, uID); return
    end
end

local last_nuke = 0 -- frame
function widget:StockpileChanged(uID, uDefID, playerID, weaponNum, oldCount, newCount)
    if UnitDefs[uDefID].customParams.unitgroup == "nuke" then
        if oldCount > newCount then
            if Spring.GetGameSeconds() <= last_nuke then return end
            local team = players_team[playerID]
            if team <= NUM_TEAMS_SUPPORTED then
                create_chat_msg("Team " .. GREEK_ALPHABET[team+1] .. " fired a nuke.")
                add_audio_stack("team_" .. GREEK_ALPHABET[team+1] .. "_is_firing_a_nuke.wav")
            else
                create_chat_msg("Player " .. tostring(playerID +1) .. " fired a nuke.")
                add_audio_stack("nuclear_weapon_fired.wav")
            end
            last_nuke = Spring.GetGameSeconds() + GLOBAL_CD
        end
    end
end


function  widget:PlayerRemoved(playerID, reason)
    -- REVIEW May need to check game state
    local players = PlayersInAllyTeamID(select(5,get_player_info(playerID, false)))
    for _, player in pairs (players) do
        if tostring(player) then
            create_chat_msg("Player " .. tostring(playerID +1) .. " removed for: " .. tostring(reason))
            add_audio_stack("player_" .. tostring(playerID +1) .. "_resigned.wav")
        end
    end
end

function widget:GameOver(winningTeam)
    -- TODO Add winning / game end handling.
    print(winningTeam) -- A integer[] of all players winning
    add_audio_stack(choice({"round_completed.wav", "game_ended.wav", "game_finished.wav"}))
end
-- !SECTION RUNTIME