import os
import wave
import contextlib

def get_audio_length(file_path):
    """Get the playback length of a .wav file in seconds."""
    with contextlib.closing(wave.open(file_path, 'r')) as wav_file:
        frames = wav_file.getnframes()
        rate = wav_file.getframerate()
        duration = frames / float(rate)
    return duration

def generate_lua_table(directory):
    """Generate a Lua table with filenames and playback lengths."""
    lua_table = "return {\n"
    for file_name in os.listdir(directory):
        if file_name.endswith('.wav'):
            file_path = os.path.join(directory, file_name)
            try:
                duration = get_audio_length(file_path)
                lua_table += f'    ["{file_name}"] = {duration:.2f},\n'
            except Exception as e:
                print(f"Error processing file {file_name}: {e}")
    lua_table += "}\n"
    return lua_table

if __name__ == "__main__":
    directory = os.path.dirname(os.path.abspath(__file__))
    lua_table = generate_lua_table(directory)
    with open(os.path.join(directory, "index.lua"), 'w') as lua_file:
        lua_file.write(lua_table)
    print("Lua table written to 'index.lua'")